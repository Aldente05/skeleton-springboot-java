package skeleton.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import skeleton.core.config.RepositoryConfig;

/**
 * Created by f.putra on 1/22/21.
 */
@SpringBootApplication(scanBasePackages = {"skeleton.core"})
@Import({RepositoryConfig.class})
public class Core {
    public static Logger getLogger(Object o) {
        return LogManager.getLogger(o.getClass());
    }
}
