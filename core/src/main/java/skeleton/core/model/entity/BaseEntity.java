package skeleton.core.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by f.putra on 22/11/19.
 */
@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    private static final long serialVersionUID = -6326503773546593654L;

    @Column(name="created_dtm")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdDtm = new Date();

    @Column(name="created_by")
    protected String createdBy;

    @Column(name="update_by")
    protected String updateBy;

    @Column(name="update_dtm")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date updateDtm = new Date();

    @Column(name="isDelete", nullable = false)
    protected boolean isDelete = false;

}
