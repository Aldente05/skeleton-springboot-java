package skeleton.core.model.log;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import skeleton.core.model.entity.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by f.putra on 1/12/21.
 */
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "LOG_ACTIVITY")
public class ActivityLog extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 4577272204006973181L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    private String id;
    @Column(name = "TABLE_NAME")
    private String TABLE_NAME;
    @Column(name = "ACTION")
    private String ACTION;
    @Column(name = "ROW_ID")
    private String ROW_ID;
    @Column(name = "OLD_DATA")
    private String OLD_DATA;
    @Column(name = "NEW_DATA")
    private String NEW_DATA;
}
