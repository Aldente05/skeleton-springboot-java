package skeleton.core.model.log;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import skeleton.core.model.entity.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by f.putra on 1/12/21.
 */
@Getter
@Setter
@Entity
@Table(name = "LOG_ERROR")
public class ErrorLog extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -505500809748991451L;

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    private String id;
    @Column(name = "USERNAME")
    private String USERNAME;
    @Column(name = "ERROR_MESSAGE")
    private String ERROR_MESSAGE;
    @Column(name = "ERROR_SOURCE")
    private String ERROR_SOURCE;
    @Column(name = "ERROR_TIME")
    private Date ERROR_TIME;

}
