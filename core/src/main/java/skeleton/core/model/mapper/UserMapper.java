package skeleton.core.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import skeleton.core.model.dto.response.UserResponse;
import skeleton.core.model.entity.MasterUser;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserResponse toUserResponse(MasterUser user);
}
