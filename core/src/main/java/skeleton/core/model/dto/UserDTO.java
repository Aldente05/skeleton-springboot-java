package skeleton.core.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by f.putra on 9/24/20.
 */
@Getter
@Setter
@NoArgsConstructor
public class UserDTO implements Serializable {

    private static final long serialVersionUID = -2482691118001704177L;

    private String fullname;
    private String username;
    private String password;
    private String email;
    private String phone;
    private String nik;
    private String kodeJabatan;
    private String jabatan;
    private String lokasi;
    private String region;
    private String company;
    private String businessArea;
    private String afdeling;
    private String blok;
    private String directLeader;
    private Date effectiveDate = null;
    private Date lastLoginAt = null;
    private String role;
    private Boolean isDelete;
}
