package skeleton.core.model.dto.master;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import skeleton.core.model.dto.BaseDTO;

import java.io.Serializable;

/**
 * Created by f.putra on 10/6/20.
 */
@AllArgsConstructor
@Getter
@Setter
public class MasterUserDTO extends BaseDTO implements Serializable {

    private static final long serialVersionUID = -6213819890101939165L;

    private String userId;
    private String email;
    private boolean is_active;
    private String username;
}
