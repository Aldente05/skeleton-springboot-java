package skeleton.core.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import skeleton.core.Core;
import skeleton.core.common.ResourceBundle;
import skeleton.core.config.RoutingDataSource;
import skeleton.core.services.LogServices;

/**
 * Created by f.putra on 3/12/20.
 */
@Aspect
@Component
@Order(0)
public class AspecService {

    @Autowired
    LogServices logServices;
    @Autowired
    ResourceBundle resourceBundle;

    @Around("@annotation(transactional)")
    public Object proceed(ProceedingJoinPoint proceedingJoinPoint, Transactional transactional) throws Throwable {
        try {
            if (transactional.readOnly()) {
                RoutingDataSource.setReplicaRoute();
//                logger.info("Routing database call to the read replica");
            } else {
                Core.getLogger(this).info(transactional.value());
            }
            return proceedingJoinPoint.proceed();
        } catch (DuplicateKeyException e) {
            Core.getLogger(this).error(transactional.transactionManager());
            throw new DuplicateKeyException(resourceBundle.getMessage("err.data.duplicate"));
        } catch (DataIntegrityViolationException e) {
            Core.getLogger(this).error(transactional.transactionManager());
            throw new DataIntegrityViolationException(resourceBundle.getMessage("err.data.existed"));
        } catch (DataAccessException e) {
            Core.getLogger(this).error(transactional.transactionManager());
            throw e;
        } finally {
            RoutingDataSource.clearReplicaRoute();
        }
    }

//    @After(value = "execution(* com.tap.core.services.*(..)))")
//    public void afterAdvice(JoinPoint joinPoint) {
//        System.out.println("After method:" + joinPoint.getSignature());
//
////        System.out.println("Successfully created Employee with name - " + name + " and id - " + empId);
//    }
}
