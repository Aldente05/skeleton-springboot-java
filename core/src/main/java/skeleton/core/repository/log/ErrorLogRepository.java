package skeleton.core.repository.log;

import skeleton.core.model.log.ErrorLog;
import skeleton.core.repository.BaseRepository;

/**
 * Created by f.putra on 1/12/21.
 */
public interface ErrorLogRepository extends BaseRepository<ErrorLog, String> {
}
