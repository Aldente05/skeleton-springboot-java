package skeleton.core.repository.log;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import skeleton.core.model.log.LoginLog;
import skeleton.core.repository.BaseRepository;

import java.util.Optional;

/**
 * Created by f.putra on 1/12/21.
 */
public interface LoginLogRepository extends BaseRepository<LoginLog, String> {

    @Query("SELECT ll FROM LoginLog ll WHERE ll.USERNAME = :username AND ll.TIME_LOGOUT IS NULL")
    Optional<LoginLog> findUser(@Param("username") String username);
}
