package skeleton.core.repository.log;


import skeleton.core.model.log.ActivityLog;
import skeleton.core.repository.BaseRepository;

/**
 * Created by f.putra on 1/12/21.
 */
public interface ActivityLogRepository extends BaseRepository<ActivityLog, String> {
}
