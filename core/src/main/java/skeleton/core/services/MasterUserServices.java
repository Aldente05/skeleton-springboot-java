package skeleton.core.services;

import com.tap.core.Core;
import com.tap.core.common.*;
import com.tap.core.model.dto.UserDTO;
import com.tap.core.model.dto.response.UserMenuResponse;
import com.tap.core.model.dto.response.UserResponse;
import com.tap.core.model.entity.*;
import com.tap.core.model.log.LoginLog;
import com.tap.core.model.mapper.PermissionMapper;
import com.tap.core.model.mapper.UserMapper;
import com.tap.core.repository.*;
import com.tap.integration.services.TokenServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by f.putra on 9/8/20.
 */
@Service
public class MasterUserServices extends BaseCriteriaBuilder implements UserDetailsService {
    @Autowired
    MasterUserRepository userRepository;

    @Autowired
    MasterRoleRepository roleRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired
    MasterMenuRepository masterMenuRepository;

    @Autowired
    MenuRoleRepository menuRoleRepository;

    @Autowired
    TokenServices tokenServices;

    @Autowired
    MenuRoleServices menuRoleServices;
    @Autowired
    LogServices logServices;
    @Autowired
    ResourceBundle resourceBundle;

    public UserResponse registerUser(UserDTO userDTO) {
        String email = userDTO.getEmail();
        String username = userDTO.getUsername();
        String nik = userDTO.getNik();

        if (!StringUtils.isBlank(nik)) {
            if (userRepository.existsByUsernameAndEmailAndNIK(email, username, nik)) {
                throw new IllegalArgumentException(resourceBundle.getMessage("err.user.validation.data_existed"));
            }
        }

        if (userRepository.existsByUsernameOrEmail(username, email))
            throw new IllegalArgumentException(resourceBundle.getMessage("err.user.validation.data_existed"));

        MasterUser user = new MasterUser();
        user.setUsername(userDTO.getUsername());
        user.setEMPLOYEE_NIK(userDTO.getNik());
        user.setEmail(email);
        user.setEMPLOYEE_FULLNAME(userDTO.getFullname());
        user.setPassword(StringUtils.encryptPassword(userDTO.getPassword()));
        user.setAfdCode(userDTO.getAfdeling());
        user.setRegionCode(userDTO.getRegion());
        user.setBlockCode(userDTO.getBlok());
        user.setCompCode(userDTO.getCompany());
        user.setDirectLeader(userDTO.getDirectLeader());
        user.setEffectiveDate(new Timestamp(userDTO.getEffectiveDate().getTime()));
        user.setEstCode(userDTO.getBusinessArea());
        user.setLokasi(userDTO.getLokasi());
        user.setJobCode(userDTO.getKodeJabatan());
        user.setJobName(userDTO.getJabatan());
        user.setCreatedBy(ContextHolder.getRequestAuthor() != null ? ContextHolder.getRequestAuthor().getUsername() : "System");
        user.setCreatedDtm(new Date());
        user.setActive(userDTO.getIsDelete());
        user.setDelete(userDTO.getIsDelete());

        user = userRepository.save(user);
        UserRole userRole = new UserRole();
        userRole.setUserId(user.getUserId());

        if (userDTO.getRole() != null && !userDTO.getRole().isEmpty()) {
            MasterRole role = roleRepository.findById(userDTO.getRole()).get();
            userRole.setRoleId(role.getRoleId());
            userRole.setMasterRole(role);
        } else {
            MasterRole role = roleRepository.findByRoleName("USER");
            userRole.setRoleId(role.getRoleId());
            userRole.setMasterRole(role);
        }

        try {
            userRoleRepository.save(userRole);
        } catch (DataIntegrityViolationException e) {
            throw new IllegalArgumentException(resourceBundle.getMessage("err.user.validation.data_existed"));
        }
        return UserMapper.INSTANCE.toUserResponse(user);
    }

    public UserResponse updateUser(String id, UserDTO dto) {
        Optional<MasterUser> optional = userRepository.findById(id);
        if (!optional.isPresent()) {
            throw new UsernameNotFoundException("User not found.");
        }
        MasterUser user = optional.get();
        user.setUsername(dto.getUsername());
        user.setEmail(dto.getEmail());
        user.setEMPLOYEE_NIK(dto.getNik());
        user.setEMPLOYEE_FULLNAME(dto.getFullname());
        user.setRegionCode(dto.getRegion());
        user.setCompCode(dto.getCompany());
        user.setEstCode(dto.getBusinessArea());
        user.setAfdCode(dto.getAfdeling());
        user.setBlockCode(dto.getBlok());
        user.setDirectLeader(dto.getDirectLeader());
        user.setEffectiveDate(dto.getEffectiveDate() == null ? null : new Timestamp(dto.getEffectiveDate().getTime()));
        user.setLokasi(dto.getLokasi());
        user.setJobCode(dto.getKodeJabatan());
        user.setJobName(dto.getJabatan());
        user.setUpdateBy(ContextHolder.getRequestAuthor() != null ? ContextHolder.getRequestAuthor().getUsername() : "System");
        user.setUpdateDtm(new Date());
        user.setActive(dto.getIsDelete() != null ? dto.getIsDelete() : true);
        user.setDelete(dto.getIsDelete() != null ? dto.getIsDelete() : true);

        try {
            user = userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new IllegalArgumentException(resourceBundle.getMessage("err.user.validation.data_existed"));
        }

        UserRole userRole = userRoleRepository.findByUserId(user.getUserId());
        if (userRole == null) userRole = new UserRole();

        MasterRole role;
        if (dto.getRole() != null && !dto.getRole().isEmpty()) {
            role = roleRepository.findById(dto.getRole()).get();
        } else {
            role = roleRepository.findByRoleName("USER");
        }

        userRole.setUserId(user.getUserId());
        userRole.setRoleId(role.getRoleId());
        userRole.setMasterRole(role);

        try {
            userRoleRepository.save(userRole);
        } catch (DataIntegrityViolationException e) {
            throw new IllegalArgumentException(resourceBundle.getMessage("err.user.validation.data_existed"));
        }
        return UserMapper.INSTANCE.toUserResponse(user);

    }

    /**
     * load user for token JWT
     *
     * @param username
     * @return
     */
    @Transactional(readOnly = true)
    public UserPrincipal loadUserByUsername(String username) {
        MasterUser user = userRepository.getByUsername(username);
        if (null == user) {
            throw new UsernameNotFoundException("User " + username + " not found.");
        }

        return new UserPrincipal(user);
    }

    /**
     * get user By user id
     *
     * @param id
     * @return
     */
    @Transactional(readOnly = true)
    public MasterUser getUserById(String id) {
        try {
            return userRepository.getUser(id);
        } catch (NoResultException e) {
            throw e;
        } catch (Exception e) {
            Core.getLogger(this).error(e.getMessage());
            throw e;
        }
    }

    /**
     * get all user
     *
     * @return
     * @throws Exception
     */
    @Transactional(readOnly = true)
    public Page<MasterUser> getAllUser(Pageable pageable, UserDTO userDTO) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<MasterUser> cq = cb.createQuery(MasterUser.class);

        Root<MasterUser> from = cq.from(MasterUser.class);
        Join<MasterUser, MasterRole> role = from.join("masterRole", JoinType.LEFT);
        applyOrderByCreatedAndUpdateDate(cb, from, cq);

        List<Predicate> predicates = new ArrayList<>();
        GenericSpesification genericSpesification = new GenericSpesification<MasterUser>();
        if (!StringUtils.isBlank(userDTO.getNik())) {
            predicates.add(cb.or(
                    cb.like(cb.upper(from.get("EMPLOYEE_NIK")), "%".concat(userDTO.getNik().toUpperCase().concat("%"))),
                    cb.like(cb.upper(from.get("EMPLOYEE_FULLNAME")), "%".concat(userDTO.getNik().toUpperCase().concat("%")))
            ));
        }
        if (!StringUtils.isBlank(userDTO.getFullname())) {
            predicates.add(cb.like(cb.lower(from.get("EMPLOYEE_FULLNAME")), "%".concat(userDTO.getFullname().toLowerCase().concat("%"))));
        }
        if (!StringUtils.isBlank(userDTO.getRegion())) {
            predicates.add(cb.like(from.get("regionCode"), "%".concat(userDTO.getRegion().toUpperCase().concat("%"))));
        }
        if (!StringUtils.isBlank(userDTO.getCompany())) {
            predicates.add(cb.like(from.get("compCode"), "%".concat(userDTO.getCompany().toUpperCase().concat("%"))));
        }
        if (!StringUtils.isBlank(userDTO.getBusinessArea())) {
            predicates.add(cb.like(from.get("estCode"), "%".concat(userDTO.getBusinessArea().toUpperCase().concat("%"))));
        }
        if (!StringUtils.isBlank(userDTO.getAfdeling())) {
            predicates.add(cb.like(from.get("afdCode"), "%".concat(userDTO.getAfdeling()).toUpperCase().concat("%")));
        }
        if (!StringUtils.isBlank(userDTO.getBlok())) {
            predicates.add(cb.like(from.get("blockCode"), "%".concat(userDTO.getBlok()).toUpperCase().concat("%")));
        }
        if (userDTO.getEffectiveDate() != null) {
            Date startDate = userDTO.getEffectiveDate();
            Date endDate = DateUtils.addDays(startDate, 1);
            predicates.add(cb.between(from.get("effectiveDate"), startDate, endDate));
        }
        if (!StringUtils.isBlank(userDTO.getRole())) {
            predicates.add(cb.like(cb.upper(role.get("roleName")), userDTO.getRole().toUpperCase()));
        }
        if (userDTO.getIsDelete() != null) {
            predicates.add(cb.equal(from.get("isDelete"), userDTO.getIsDelete()));
        }

        cq.where(predicates.toArray(new Predicate[0]));

        TypedQuery<MasterUser> query = entityManager.createQuery(cq)
                .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                .setMaxResults(pageable.getPageSize());

        long totalRows = userRepository.count(genericSpesification);

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public void logout(String userId) {
        MasterUser user = this.getUserById(userId);
        user.setLastLoginAt(null); // reset last login
        try {
            logServices.updateLoginLog(new LoginLog(user.getUserId(), user.getUsername(), null, null, null, null, null, "LOGIN"));
            userRepository.save(user);
        } catch (Exception e) {
            throw e;
        }
    }

    private MasterUser userMapping(MasterUser queryResult) {

        MasterUser result = new MasterUser();
        result.setUsername(queryResult.getUsername());
        result.setEMPLOYEE_FULLNAME(queryResult.getEMPLOYEE_FULLNAME());
        result.setEmail(queryResult.getEmail());
        return result;
    }

    public UserResponse deleteUser(String id) {
        Optional<MasterUser> optional = userRepository.findById(id);
        if (!optional.isPresent()) {
            throw new UsernameNotFoundException("User not found.");
        }

        MasterUser user = optional.get();
        user.setActive(false);
        user.setDelete(true);
        user.setUpdateBy(ContextHolder.getRequestAuthor().getId());
        user.setUpdateDtm(new Date());
        return UserMapper.INSTANCE.toUserResponse(userRepository.save(user));
    }

    public UserResponse getMenuRoleAfterLogin(String userId) {
        UserResponse userResponse = UserMapper.INSTANCE.toUserResponse(getUserById(userId));
        boolean roleActive = roleActive(userResponse.getRole().getRoleId());
        if (!roleActive)
            throw new RestClientException(resourceBundle.getMessage("err.role.validation.inactive", userResponse.getRole().getName()));

        List<MenuRole> menuRoles = menuRoleServices.getAllByRole(userResponse.getRole().getRoleId());
        List<UserMenuResponse> menus = new ArrayList<>();
        getRoot(menuRoles, menus);
        for (UserMenuResponse menu : menus) {
            createMenuTree(menuRoles, menu);
            menu.getViews().sort(Comparator.comparingInt(UserMenuResponse::getOrderMenu));
        }
        userResponse.getRole().setMenus(menus);
        return userResponse;
    }

    public UserResponse getUserProfile() {
        return getMenuRoleAfterLogin(ContextHolder.getRequestAuthor().getId());
    }

    private void getRoot(List<MenuRole> menus, List<UserMenuResponse> userMenuResponses) {
        for (MenuRole iterator : menus) {
            MasterMenu menu = iterator.getMenu();
            if (menu.getParent().equals("0")) {
                // After obtaining the root node information, delete the root node information from the list
//                menus.remove(iterator);
                userMenuResponses.add(PermissionMapper.INSTANCE.toResponse(iterator));
            }
        }
        //sorting parent menu
        userMenuResponses.sort(Comparator.comparingInt(UserMenuResponse::getOrderMenu));
    }

    private void createMenuTree(List<MenuRole> menus, UserMenuResponse parentNode) {
        //This is judged here to prevent access to an empty root node and cause access to the root node information.
        // Generate an exception

        if (null != parentNode) {
            for (Iterator<MenuRole> it = menus.iterator(); it.hasNext(); ) {
                MenuRole iterator = it.next();
                MasterMenu menu = iterator.getMenu();
                MasterMenu parentMenu = masterMenuRepository.findById(parentNode.getMenuId()).get();
                if (parentMenu.getMenuId().equals(menu.getParent())) {
                    // Need to remove the information here from the list, which can reduce the number of subsequent list traversal
                    menus.remove(iterator);
                    UserMenuResponse node = PermissionMapper.INSTANCE.toResponse(iterator);
                    parentNode.getViews().add(node);
                    // Recursively continue to find the child nodes of the child nodes in the remaining list
                    //The condition returned by recursion is that no child nodes are found in the remaining list.
                    createMenuTree(menus, node);
                    //Require the iterator because the list has changed
                    it = menus.iterator();
                }
            }
        }
    }

    public boolean roleActive(String roleId) {
        Optional<MasterRole> optional = roleRepository.findById(roleId);
        return optional.map(role -> !role.isDelete()).orElse(true);
    }

    public void saveLastLogin(String id, Date loginAt) {
        MasterUser user = getUserById(id);
        if (user.getLastLoginAt() != null) {
            throw new RestClientException(resourceBundle.getMessage("err.user.validation.logged"));
        }
        user.setLastLoginAt(loginAt);
        userRepository.save(user);
    }

    public List<MasterUser> checkUserLogin() {
        return userRepository.findByLastLoginAtIsNotNull();
    }

    public MasterUser saveUser(MasterUser user) {
        return userRepository.save(user);
    }
}
