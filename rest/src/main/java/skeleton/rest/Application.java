package skeleton.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

/**
 * Created by f.putra on 1/22/21.
 */
@SpringBootApplication(scanBasePackages = {"skeleton.core", "skeleton.rest"}, exclude = {SecurityAutoConfiguration.class})
public class Application {
    public static Logger getLogger(Object o) {
        return LogManager.getLogger(o.getClass());
    }

    public static void main(String[] args) {
        try {
            SpringApplication.run(Application.class, args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostConstruct
    void started() {
        // set JVM timezone as UTC
        TimeZone.setDefault(TimeZone.getTimeZone("Bangkok/Jakarta"));
    }
}
